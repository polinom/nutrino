# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0004_auto_20150614_2338'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='recipetoingredients',
            name='unit',
        ),
        migrations.AlterField(
            model_name='recipetoingredients',
            name='amount',
            field=models.IntegerField(help_text=b'Amount in grams'),
        ),
    ]
