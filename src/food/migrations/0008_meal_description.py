# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0007_auto_20150814_0125'),
    ]

    operations = [
        migrations.AddField(
            model_name='meal',
            name='description',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
