# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DataDerivation',
            fields=[
                ('code', models.CharField(max_length=4, serialize=False, verbose_name='Code', primary_key=True)),
                ('description', models.CharField(help_text='Description of derivation code giving specific information on how the value was determined.', max_length=120, verbose_name='Description')),
            ],
            options={
                'ordering': ['code'],
                'verbose_name': 'Data Derivation',
                'verbose_name_plural': 'Data Derivations',
            },
        ),
        migrations.CreateModel(
            name='DataSource',
            fields=[
                ('id', models.CharField(help_text='Unique number identifying the reference/source.', max_length=6, serialize=False, verbose_name='Id', primary_key=True)),
                ('authors', models.CharField(help_text='List of authors for a journal article or name of sponsoring organization for other documents.', max_length=255, verbose_name='Authors', blank=True)),
                ('title', models.CharField(help_text='Title of article or name of document, such as a report from a company or trade association.', max_length=255, verbose_name='Title')),
                ('year', models.IntegerField(help_text='Year article or document was published.', null=True, verbose_name='Year', blank=True)),
                ('journal', models.CharField(help_text='Name of the journal in which the article was published.', max_length=135, null=True, verbose_name='Journal', blank=True)),
                ('volume_or_city', models.CharField(help_text='Volume number for journal articles, books, or reports; city where sponsoring organization is located.', max_length=16, verbose_name='Volume or City', blank=True)),
                ('issue_or_state', models.CharField(help_text='Issue number for journal article; State where the sponsoring organization is located.', max_length=5, verbose_name='Issue or State', blank=True)),
                ('start_page', models.IntegerField(help_text='Starting page number of article/document.', null=True, verbose_name='Start Page', blank=True)),
                ('end_page', models.IntegerField(help_text='Ending page number of article/document.', null=True, verbose_name='End Page', blank=True)),
            ],
            options={
                'ordering': ['id'],
                'verbose_name': 'Data Source',
                'verbose_name_plural': 'Data Sources',
            },
        ),
        migrations.CreateModel(
            name='Food',
            fields=[
                ('ndb_number', models.IntegerField(help_text='Nutrient Databank number that uniquely identifies a food item.', serialize=False, verbose_name='Nutrient Databank Number', primary_key=True)),
                ('long_description', models.CharField(help_text='Description of food item', max_length=200, verbose_name='Long Description', blank=True)),
                ('short_description', models.CharField(help_text='Description of food item', max_length=60, verbose_name='Short Description', blank=True)),
                ('common_name', models.CharField(help_text='Other names commonly used to describe a food, including local or regional names for various foods, for example, "soda" or "pop" for "carbonated beverages."', max_length=100, verbose_name='Common Name', blank=True)),
                ('manufacturer_name', models.CharField(help_text='Indicates the company that manufactured the product, when appropriate.', max_length=65, verbose_name='Manufacturer Name', blank=True)),
                ('survey', models.BooleanField(default=False, help_text='Indicates if the food item is used in the USDA Food and Nutrient Database for Dietary Studies (FNDDS) and thus has a complete nutrient profile for the 65 FNDDS nutrients.', verbose_name='Survey')),
                ('refuse_description', models.CharField(help_text='Description of inedible parts of a food item (refuse), such as seeds or bone.', max_length=135, verbose_name='Refuse Description', blank=True)),
                ('refuse_percentage', models.IntegerField(help_text='Percentage of refuse.', null=True, verbose_name='Refuse Percentage', blank=True)),
                ('scientific_name', models.CharField(help_text='Scientific name of the food item. Given for the least processed form of the food (usually raw), if applicable.', max_length=65, verbose_name='Scientific Name', blank=True)),
                ('nitrogen_factor', models.FloatField(help_text='Factor for converting nitrogen to protein.', null=True, verbose_name='Nitrogen Factor', blank=True)),
                ('protein_factor', models.FloatField(help_text='Factor for calculating calories from protein.', null=True, verbose_name='Protein Factor', blank=True)),
                ('fat_factor', models.FloatField(help_text='Factor for calculating calories from fat.', null=True, verbose_name='Fat Factor', blank=True)),
                ('cho_factor', models.FloatField(help_text='Factor for calculating calories from carbohydrate.', null=True, verbose_name='CHO Factor', blank=True)),
            ],
            options={
                'ordering': ['ndb_number'],
                'verbose_name': 'Food',
                'verbose_name_plural': 'Foods',
            },
        ),
        migrations.CreateModel(
            name='FoodGroup',
            fields=[
                ('code', models.IntegerField(help_text='Code identifying a food group. Codes may not be consecutive.', serialize=False, verbose_name='Food Group Code', primary_key=True)),
                ('description', models.CharField(help_text='Name of food group.', max_length=60, verbose_name='Description', blank=True)),
            ],
            options={
                'ordering': ['code'],
                'verbose_name': 'Food Group',
                'verbose_name_plural': 'Food Groups',
            },
        ),
        migrations.CreateModel(
            name='Footnote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField(help_text='Sequence number. If a given footnote applies to more than one nutrient number, the same footnote number is used. As a result, this file cannot be indexed.', verbose_name='Sequence')),
                ('type', models.CharField(help_text='Type of footnote.', max_length=1, verbose_name='Type', choices=[(b'D', 'Footnote adding information to the food  description'), (b'M', 'Footnote adding information to measure description'), (b'N', 'Footnote providing additional information on a nutrient value')])),
                ('text', models.CharField(help_text='Footnote text.', max_length=200, verbose_name='Text')),
                ('food', models.ForeignKey(verbose_name='Food', to='usda.Food')),
            ],
            options={
                'ordering': ['food', 'number'],
                'verbose_name': 'Footnote',
                'verbose_name_plural': 'Footnotes',
            },
        ),
        migrations.CreateModel(
            name='Nutrient',
            fields=[
                ('number', models.IntegerField(help_text='Unique identifier code for a nutrient.', serialize=False, verbose_name='Number', primary_key=True)),
                ('units', models.CharField(help_text='Units of measure (mg, g, and so on).', max_length=7, verbose_name='Units')),
                ('tagname', models.CharField(help_text='International Network of Food Data Systems (INFOODS) Tagnames. A unique abbreviation for a nutrient/food component developed by INFOODS to aid in the interchange of data.', max_length=20, verbose_name='Tagname', blank=True)),
                ('description', models.CharField(help_text='Name of nutrient/food component.', max_length=60, verbose_name='Description')),
                ('decimals', models.IntegerField(help_text='Number of decimal places to which a nutrient value is rounded.', verbose_name='Decimals')),
                ('order', models.IntegerField(help_text='Used to sort nutrient records in the same order as various reports produced from SR.', verbose_name='Order')),
            ],
            options={
                'ordering': ['number'],
                'verbose_name': 'Nutrient',
                'verbose_name_plural': 'Nutrients',
            },
        ),
        migrations.CreateModel(
            name='NutrientData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nutrient_value', models.FloatField(help_text='Amount in 100 grams, edible portion.', verbose_name='Nutrient Value')),
                ('data_points', models.IntegerField(help_text='Number of data points (previously called Sample_Ct) is the number of analyses used to calculate the nutrient value. If the number of data points is 0, the value was calculated or imputed.', verbose_name='Data Points')),
                ('standard_error', models.FloatField(help_text='Standard error of the mean. Null if can not be calculated.', null=True, verbose_name='Standard Error', blank=True)),
                ('reference_nbd_number', models.IntegerField(help_text='NDB number of the item used to impute a missing value. Populated only for items added or updated starting with SR14.', null=True, verbose_name='Reference NBD Number', blank=True)),
                ('added_nutrient', models.BooleanField(default=False, help_text='Indicates a vitamin or mineral added for fortification or enrichment. This field is populated for ready-to-eat breakfast cereals and many brand-name hot cereals in food group 8.', verbose_name='Added Nutritient')),
                ('number_of_studies', models.IntegerField(help_text='Number of studies.', null=True, verbose_name='Number of Studies', blank=True)),
                ('minimum', models.FloatField(help_text='Minimum value.', null=True, verbose_name='Minimum', blank=True)),
                ('maximum', models.FloatField(help_text='Maximum value.', null=True, verbose_name='Maximum', blank=True)),
                ('degrees_of_freedom', models.IntegerField(help_text='Degrees of freedom.', null=True, verbose_name='Degrees of Freedom', blank=True)),
                ('lower_error_bound', models.FloatField(help_text='Lower 95% error bound.', null=True, verbose_name='Lower Error Bound.', blank=True)),
                ('upper_error_bound', models.FloatField(help_text='Upper 95% error bound.', null=True, verbose_name='Upper Error Bound.', blank=True)),
                ('statistical_comments', models.CharField(help_text='Statistical comments.', max_length=10, verbose_name='Statistical Comments', blank=True)),
                ('confidence_code', models.CharField(help_text='Confidence Code indicating data quality, based on evaluation of sample plan, sample handling, analytical method, analytical quality control, and number of samples analyzed. Not included in this release, but is planned for future releases.', max_length=1, verbose_name='Confidence Code', blank=True)),
                ('data_derivation', models.ForeignKey(blank=True, to='usda.DataDerivation', help_text='Data Derivation giving specific information on how the value is determined.', null=True, verbose_name='Data Derivation')),
                ('food', models.ForeignKey(verbose_name='Food', to='usda.Food')),
                ('nutrient', models.ForeignKey(verbose_name='Nutrient', to='usda.Nutrient')),
            ],
            options={
                'ordering': ['food', 'nutrient'],
                'verbose_name': 'Nutrient Data',
                'verbose_name_plural': 'Nutrient Data',
            },
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('code', models.IntegerField(serialize=False, verbose_name='Code', primary_key=True)),
                ('description', models.CharField(help_text='Description of source code that identifies the type of nutrient data.', max_length=60, verbose_name='Description')),
            ],
            options={
                'ordering': ['code'],
                'verbose_name': 'Source',
                'verbose_name_plural': 'Sources',
            },
        ),
        migrations.CreateModel(
            name='Weight',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sequence', models.IntegerField(help_text='Sequence number.', verbose_name='Sequence')),
                ('amount', models.FloatField(help_text='Unit modifier (for example, 1 in "1 cup").', verbose_name='Amount')),
                ('description', models.CharField(help_text='Description (for example, cup, diced, and 1-inch pieces).', max_length=80, verbose_name='Description')),
                ('gram_weight', models.FloatField(help_text='Gram weight.', verbose_name='Gram Weight')),
                ('number_of_data_points', models.FloatField(help_text='Number of data points.', null=True, verbose_name='Number of Data Points', blank=True)),
                ('standard_deviation', models.FloatField(help_text='Standard Deviation', null=True, verbose_name='Standard Deviation', blank=True)),
                ('food', models.ForeignKey(verbose_name='Food', to='usda.Food')),
            ],
            options={
                'ordering': ['food', 'sequence'],
                'verbose_name': 'Weight',
                'verbose_name_plural': 'Weights',
            },
        ),
        migrations.AddField(
            model_name='nutrientdata',
            name='source',
            field=models.ManyToManyField(help_text='Type of data', to='usda.Source', verbose_name='Source'),
        ),
        migrations.AddField(
            model_name='footnote',
            name='nutrient',
            field=models.ForeignKey(verbose_name='nutrient', blank=True, to='usda.Nutrient', null=True),
        ),
        migrations.AddField(
            model_name='food',
            name='food_group',
            field=models.ForeignKey(verbose_name='Food Group', to='usda.FoodGroup', help_text='Food group to which a food item belongs.'),
        ),
        migrations.AlterUniqueTogether(
            name='weight',
            unique_together=set([('food', 'sequence')]),
        ),
        migrations.AlterUniqueTogether(
            name='nutrientdata',
            unique_together=set([('food', 'nutrient')]),
        ),
        migrations.AlterUniqueTogether(
            name='footnote',
            unique_together=set([('food', 'number', 'nutrient')]),
        ),
    ]
