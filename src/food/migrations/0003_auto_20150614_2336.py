# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0002_auto_20150614_1941'),
    ]

    operations = [
        migrations.RenameField(
            model_name='recipetoingredients',
            old_name='product',
            new_name='recipe',
        ),
    ]
