from django.views import generic
from food.models import Meal


class HomePage(generic.TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(HomePage, self).get_context_data(**kwargs)

        context['items'] = Meal.objects.all()

        return context


class AboutPage(generic.TemplateView):
    template_name = "about.html"
