(function(){
  $(window).scroll(function () {
      var top = $(document).scrollTop();
      $('.corporate-jumbo').css({
        'background-position': '0px -'+(top/3).toFixed(2)+'px'
      });
      if(top > 50)
        $('.navbar').removeClass('navbar-transparent');
      else
        $('.navbar').addClass('navbar-transparent');
  }).trigger('scroll');


    $(document).ready(function(){
        $('.calculator-form').validate({
            rules: {
                age: {
                    required: true,
                    range: [1, 120]
                },

                weight: {
                    required: true,
                    range: [10, 500]
                },

                feet: {
                    required: true,
                    range: [1, 10]
                },

                inch: {
                    required: true,
                    range: [0, 9999]
                }
            }
        });
        $('.count-it').click(function(e){
            e.preventDefault()

            var gender = $('#gender').val();
            var age = $('#age').val();
            var weight = $('#weight').val();
            var feet = $('#feet').val();
            var inch = $('#inch').val();
            var activity = $('#activity').val();
            var goal = $('#goal').val();

            var al_inch = (parseInt(feet) * 12) + parseInt(inch);

            console.log(al_inch)
            var height = al_inch * 2.54;
            var kg = parseFloat(weight) / 2.2046;

            if(gender === 'male'){
                var BMR = (66.47+ 13.75 * kg + 5.0 * height - 6.75 * parseInt(age)) * parseFloat(activity);
                //var BMR = (88.36 + 13.4 * kg + 4.8 * height - 5.75 * parseInt(age)) * parseFloat(activity);
            } else if (gender === 'female'){
                var BMR = (665.09 + 9.56 * kg + 1.84 * height - 4.67 * parseInt(age)) * parseFloat(activity);
                //var BMR = (447.6 + 9.2 * kg + 3.1 * height - 4.3 * parseInt(age)) * parseFloat(activity);
            };

            console.log(parseInt(goal));
            BMR = BMR + parseInt(goal);

            $('.calories').text(Math.round(BMR));



        });
    })


})();