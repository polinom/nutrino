# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usda', '0001_initial'),
        ('food', '0006_recipe_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='FoodItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('fat', models.IntegerField()),
                ('protein', models.IntegerField()),
                ('carbs', models.IntegerField()),
                ('related_usda', models.ForeignKey(to='usda.Food')),
            ],
        ),
        migrations.CreateModel(
            name='FoodItemToMeal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('fooditem', models.ForeignKey(to='food.FoodItem')),
            ],
        ),
        migrations.CreateModel(
            name='Meal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to=b'')),
                ('items', models.ManyToManyField(to='food.FoodItem', through='food.FoodItemToMeal')),
            ],
        ),
        migrations.AddField(
            model_name='fooditemtomeal',
            name='meal',
            field=models.ForeignKey(to='food.Meal'),
        ),
    ]
