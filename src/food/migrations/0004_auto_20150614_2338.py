# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0003_auto_20150614_2336'),
    ]

    operations = [
        migrations.RenameField(
            model_name='recipetoingredients',
            old_name='ingredient',
            new_name='food',
        ),
    ]
