# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usda', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('makes', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='RecipeToIngredients',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('unit', models.IntegerField(choices=[(1, b'Caps'), (2, b'gr')])),
                ('ingredient', models.ForeignKey(to='usda.Food')),
                ('product', models.ForeignKey(to='food.Recipe')),
            ],
        ),
        migrations.AddField(
            model_name='recipe',
            name='ingredients',
            field=models.ManyToManyField(to='usda.Food', through='food.RecipeToIngredients'),
        ),
    ]
