from django.contrib import admin
from django.forms import ValidationError, ModelForm, ModelMultipleChoiceField
from django.contrib.admin import SimpleListFilter
from django.contrib.admin.widgets import FilteredSelectMultiple
from .models import *


class MembershipInline(admin.TabularInline):
    model = Recipe.ingredients.through
    raw_id_fields = ('food', 'recipe')


class RecipeAdmin(admin.ModelAdmin):
    inlines = (MembershipInline, )


class FoodItemToMealInlineInline(admin.TabularInline):
    model = Meal.items.through


class MealAdmin(admin.ModelAdmin):
    inlines = (FoodItemToMealInlineInline, )


class FoodItemAdmin(admin.ModelAdmin):
    models = FoodItem
    raw_id_fields = ("related_usda",)

admin.site.register(FoodItem, FoodItemAdmin)
admin.site.register(Meal, MealAdmin)
admin.site.register(Recipe, RecipeAdmin)
# Register your models here.
