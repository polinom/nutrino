# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='ingredients',
            field=models.ManyToManyField(related_name='recipes', through='food.RecipeToIngredients', to='usda.Food'),
        ),
        migrations.AlterField(
            model_name='recipetoingredients',
            name='unit',
            field=models.ForeignKey(to='usda.Weight'),
        ),
    ]
