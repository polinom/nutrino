from django.db import models
from usda.models import Food, Weight


class Recipe(models.Model):
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to='/food_images/')
    description = models.TextField()
    ingredients = models.ManyToManyField(Food, through='RecipeToIngredients', related_name='recipes')
    makes = models.IntegerField()

    def __unicode__(self):
        return self.title


class RecipeToIngredients(models.Model):

    recipe = models.ForeignKey(Recipe)
    food = models.ForeignKey(Food)
    amount = models.IntegerField(help_text="Amount in grams")


class FoodItem(models.Model):
    title = models.CharField(max_length=255)
    related_usda = models.ForeignKey(Food)
    fat = models.IntegerField()
    protein = models.IntegerField()
    carbs = models.IntegerField()

    def __unicode__(self):
        return self.title


class Meal(models.Model):
    title = models.CharField(max_length=255)
    items = models.ManyToManyField(FoodItem, through='FoodItemToMeal')
    image = models.ImageField()
    description = models.TextField()

    def __unicode__(self):
        return self.title


class FoodItemToMeal(models.Model):
    fooditem = models.ForeignKey(FoodItem)
    meal = models.ForeignKey(Meal)
    amount = models.IntegerField()



